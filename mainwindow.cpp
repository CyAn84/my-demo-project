﻿#include "mainwindow.h"
#include <cstdlib>
#include <ctime>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
/********[ MAINWINDOW SECTION ]*************************************************/

	this->setObjectName(QStringLiteral("MainWindow"));
	this->setWindowTitle(QStringLiteral("Поиск общих подстрок"));
	this->resize(800, 600);
	this->setMinimumSize(QSize(640, 480));
	this->setDockOptions(QMainWindow::AllowNestedDocks |
						 QMainWindow::AllowTabbedDocks |
						 QMainWindow::AnimatedDocks	   |
						 QMainWindow::ForceTabbedDocks);

/********[ ACTIONS SECTION ]****************************************************/

	actionQuit = new QAction(this);
	QIcon iconQuit;
    iconQuit.addFile(QStringLiteral(":/icons/quit.png"), QSize(), QIcon::Normal, QIcon::On);
	actionQuit->setObjectName(QStringLiteral("actionQuit"));
	actionQuit->setText(QStringLiteral("Выйти"));
	actionQuit->setStatusTip(QStringLiteral("Выйти из приложения"));
	actionQuit->setShortcut(Qt::ALT + Qt::Key_X);
	actionQuit->setIcon(iconQuit);

	actionGenerate = new QAction(this);
	QIcon iconGenerate;
    iconGenerate.addFile(QStringLiteral(":/icons/generate.png"), QSize(), QIcon::Normal, QIcon::On);
	actionGenerate->setObjectName(QStringLiteral("actionGenerate"));
	actionGenerate->setText(QStringLiteral("Сгенерировать"));
	actionGenerate->setStatusTip(QStringLiteral("Сгенерировать строки случайным образом"));
	actionGenerate->setShortcut(Qt::CTRL + Qt::Key_G);
	actionGenerate->setIcon(iconGenerate);

	actionSearch = new QAction(this);
	QIcon iconSearch;
    iconSearch.addFile(QStringLiteral(":/icons/search.png"), QSize(), QIcon::Normal, QIcon::On);
	actionSearch->setObjectName(QStringLiteral("actionSearch"));
	actionSearch->setText(QStringLiteral("Найти"));
	actionSearch->setStatusTip(QStringLiteral("Найти общие подстроки"));
	actionSearch->setShortcut(Qt::CTRL + Qt::Key_F);
	actionSearch->setIcon(iconSearch);
	actionSearch->setEnabled(false);

	actionPaste = new QAction(this);
	QIcon iconPaste;
    iconPaste.addFile(QStringLiteral(":/icons/paste.png"), QSize(), QIcon::Normal, QIcon::On);
	actionPaste->setObjectName(QStringLiteral("actionPaste"));
	actionPaste->setText(QStringLiteral("Вставить"));
	actionPaste->setStatusTip(QStringLiteral("Вставить из буфера обмена"));
	actionPaste->setShortcut(Qt::CTRL + Qt::Key_V);
	actionPaste->setIcon(iconPaste);

	actionOpen = new QAction(this);
	QIcon iconOpen;
    iconOpen.addFile(QStringLiteral(":/icons/open.png"), QSize(), QIcon::Normal, QIcon::On);
	actionOpen->setObjectName(QStringLiteral("actionOpen"));
	actionOpen->setText(QStringLiteral("Открыть"));
	actionOpen->setStatusTip(QStringLiteral("Открыть текстовый файл"));
	actionOpen->setShortcut(Qt::CTRL + Qt::Key_O);
	actionOpen->setIcon(iconOpen);

	actionAbout = new QAction(this);
	QIcon iconAbout;
    iconAbout.addFile(QStringLiteral(":/icons/help.png"), QSize(), QIcon::Normal, QIcon::On);
	actionAbout->setObjectName(QStringLiteral("actionAbout"));
	actionAbout->setText(QStringLiteral("О программе"));
	actionAbout->setStatusTip(QStringLiteral("О программе"));
	actionAbout->setIcon(iconAbout);

/********[ MENUBAR SECTION ]****************************************************/

	menuBar = new QMenuBar(this);
	menuBar->setObjectName(QStringLiteral("menuBar"));

	menuFile = new QMenu(menuBar);
	menuFile->setObjectName(QStringLiteral("menuFile"));
	menuFile->setTitle(QStringLiteral("Файл"));

	menuEdit = new QMenu(menuBar);
	menuEdit->setObjectName(QStringLiteral("menuEdit"));
	menuEdit->setTitle(QStringLiteral("Правка"));

	menuHelp = new QMenu(menuBar);
	menuHelp->setObjectName(QStringLiteral("menuHelp"));
	menuHelp->setTitle(QStringLiteral("Справка"));

	this->setMenuBar(menuBar);

	menuBar->addAction(menuFile->menuAction());
	menuBar->addAction(menuEdit->menuAction());
//	menuBar->addAction(menuHelp->menuAction()); // No need yet. CA 20170110
	menuFile->addAction(actionOpen);
	menuFile->addSeparator();
	menuFile->addAction(actionQuit);
	menuEdit->addAction(actionGenerate);
	menuEdit->addAction(actionPaste);
	menuEdit->addAction(actionSearch);
	menuHelp->addAction(actionAbout);

/********[ TOOLBAR SECTION ]****************************************************/

	mainToolBar = new QToolBar(this);
	mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
	mainToolBar->setWindowTitle(QStringLiteral("Панель инструментов"));
	mainToolBar->setAllowedAreas(Qt::AllToolBarAreas);
	mainToolBar->setIconSize(QSize(32, 32));
	mainToolBar->setToolButtonStyle(Qt::ToolButtonIconOnly);

	this->addToolBar(Qt::TopToolBarArea, mainToolBar);

	mainToolBar->addAction(actionGenerate);
	mainToolBar->addAction(actionOpen);
	mainToolBar->addAction(actionPaste);
	mainToolBar->addAction(actionSearch);

/********[ STATUSBAR SECTION ]**************************************************/

	statusBar = new QStatusBar(this);
	statusBar->setObjectName(QStringLiteral("statusBar"));
	this->setStatusBar(statusBar);

/********[ WIDGETS SECTION ]****************************************************/

	textEdit = new QTextEdit(this);
	textEdit->setObjectName(QStringLiteral("textEdit"));
	QFont font;
	font.setFamily(QStringLiteral("Arial"));
	font.setPointSize(10);
	textEdit->setFont(font);
	textEdit->setReadOnly(true);
//	textEdit->setFrameStyle(QFrame::WinPanel);

	this->setCentralWidget(textEdit);

	dockWidgetResult = new QDockWidget(this);
	dockWidgetResult->setObjectName(QStringLiteral("dockWidgetResult"));
	dockWidgetResult->setWindowTitle(QStringLiteral("Результаты поиска"));
	dockWidgetResult->setFeatures(QDockWidget::DockWidgetFloatable | QDockWidget::DockWidgetMovable);

	widgetResult = new QTreeWidget(dockWidgetResult);
	widgetResult->setObjectName(QStringLiteral("widgetResult"));
	QStringList hdr;
	hdr << QStringLiteral("Подстрока") << QStringLiteral("Длина");
	widgetResult->setHeaderLabels(hdr);
	widgetResult->setSortingEnabled(true);
	widgetResult->setRootIsDecorated(false);

	dockWidgetResult->setWidget(widgetResult);

	this->addDockWidget(static_cast<Qt::DockWidgetArea>(8), dockWidgetResult);

/********[ CONNECTIONS SECTION ]************************************************/

	connect(actionGenerate,			SIGNAL(triggered()),
			this,					SLOT(generateClicked()));

	connect(actionSearch,			SIGNAL(triggered()),
			this,					SLOT(searchClicked()));

	connect(textEdit,				SIGNAL(textChanged()), // Activate checkbox when the Generate button pushed.
			this,					SLOT(enableSearchButton()));

	connect(widgetResult,			SIGNAL(itemClicked(QTreeWidgetItem*, int)),
			this,					SLOT(resultClicked(QTreeWidgetItem*, int)));

	connect(actionPaste,			SIGNAL(triggered()),
			this,					SLOT(pasteClicked()));

	connect(actionOpen,				SIGNAL(triggered()),
			this,					SLOT(openFileClicked()));

	connect(actionQuit,				SIGNAL(triggered()),
			this,					SLOT(close()));

	QMetaObject::connectSlotsByName(this);
}

MainWindow::~MainWindow()
{
//	Not needed
}

/***************[ METHODS ]***************************************************/

void MainWindow::generateStrings()
{
	inputStringArray.clear();
	static const char alphabet[] = "aaabbc";
	int alphabetLength = sizeof(alphabet) - 1;
	srand(time(0));
	int arraySize = rand() % 200 + 30;
	int stringSize = 1;
	for (int j = 0; j < arraySize; j++)
	{
		stringSize = rand() % 200 + 50;
		QString tmpStr;
		for (int i = 0; i < stringSize; i++)
		{
			tmpStr.append(alphabet[rand() % alphabetLength]);
		}
		inputStringArray.append(tmpStr);
	}
}

int MainWindow::getCommonSubstring()
{
	// Check if not empty
	// Compare symbol-by-symbol
	// Results to temp-array if more than 1 symbols are the same
	// Delete duplicates in temp-array
	// Compare each temp-array member with rest-of-input-array
	// If mached then => result (commonSubstringArray)
	// Check if result is not empty and return 0, otherwise 1

	if (!(inputStringArray.isEmpty()))
	{
		QString a = inputStringArray.at(0); // Get the first string to compare
		QString b = inputStringArray.at(1); // Get the second string to compare
		const int a_size = a.size(); 
		const int b_size = b.size();
		const int solution_size = b_size + 1;
		QVector<int> x(solution_size, 0);
		QVector<int> y(solution_size, 0);
		QVector<int> *previous = &x;
		QVector<int> *current = &y;
		QStringList temp; // Temporary result.
		int max_length = 0;
		int result_index = 0;
		for (int i = a_size - 1; i >= 0; i--)
		{
			for (int j = b_size - 1; j >= 0; j--)
			{
				if (a[i] == b[j])
				{
					(*current)[j] = 1 + (*previous)[j + 1];
					if ((*current)[j] > 1)
					{
						temp.push_back(a.mid(i, (*current)[j]));
						if ((*current)[j] > 2)
						{
							int count = (*current)[j];
							while (count > 2)
							{
								temp.push_back(a.mid(i, count - 1));
								count--;
							}
						}
					}
				}
				else
				{
					(*current)[j] = 0;
				}
			}
			std::swap(previous, current);
		}
		temp.removeDuplicates();
// Comparing results of first 2 strings for all strings
		commonSubstringArray.clear();
		int gotEnd = 0;
		for (QStringList::Iterator its = temp.begin(); its != temp.end(); its++)
		{
			gotEnd = 0; // Reach the end of inputArray?
			QStringList::Iterator itv = inputStringArray.begin();
			while ((itv != inputStringArray.end()) && (itv->contains(*its)))
			{
				itv++;
				if (itv == inputStringArray.end())
				{
					gotEnd = 1;
				}
			}
			if ((!(commonSubstringArray.contains(*its))) && (gotEnd == 1))
			{
				commonSubstringArray.append(*its);
			}
		}
		if (!(commonSubstringArray.isEmpty())) // If not empty return OK
		{
			return 0;
		}
		else return 1;
	}
	else
	{
		return 1;
	}
}

/***************[SLOTS]*******************************************************/

void MainWindow::generateClicked()
{
	QGuiApplication::setOverrideCursor(Qt::WaitCursor);

	generateStrings();
	this->textEdit->clear();
	this->widgetResult->clear();
	isFirstTime = true; // Trigger for correct UNDO.
	QStringList::Iterator it;
	for (it = inputStringArray.begin(); it != inputStringArray.end(); it++)
	{
		this->textEdit->append(*it);
	}

	QGuiApplication::restoreOverrideCursor();

	this->actionSearch->setEnabled(true);
}

void MainWindow::pasteClicked()
{
	// Validate if strings number > 1

	QClipboard *pcb = QApplication::clipboard();
	// paste strings into inputStringArray
	QString tmpStr = pcb->text();
	inputStringArray.clear();
	inputStringArray = tmpStr.split('\n');
	// paste strings into textEdit field
	this->textEdit->clear();
	this->widgetResult->clear();
	for (QStringList::Iterator it = inputStringArray.begin(); it != inputStringArray.end(); ++it)
	{
		this->textEdit->append(*it);
	}
	if ((inputStringArray.size()) < 2)
	{
		msgBox.setWindowTitle(QStringLiteral("Внимание!"));
		msgBox.setText(QStringLiteral("Мало срок для поиска!"));
		msgBox.exec();
	}
	else
	{
		this->actionSearch->setEnabled(true);
	}
}

void MainWindow::openFileClicked()
{
	// Open TEXT file, if done cont, otherwise message
	// validate it has srings, otherwise message
	// remove empty strings if there are
	// File => inputStringArray

	QString fileName = QFileDialog::getOpenFileName(this,
													QStringLiteral("Открыть файл"),
													QString(),
													QStringLiteral("Text Files (*.txt)"));
	if (!fileName.isEmpty())
	{
		QFile inputFile(fileName);
		if (!inputFile.open(QIODevice::ReadOnly))
		{
			QMessageBox::critical(this, QStringLiteral("Ошибка"), QStringLiteral("Не удаётся открыть файл"));
			return;
		}
		QTextStream textStream(&inputFile);
		inputStringArray.clear();
		while (true)
		{
			QString line = textStream.readLine();
			if (line.isNull())
			{
				break;
			}
			else
			{
				inputStringArray.append(line);
			}
		}
		inputFile.close();
		this->textEdit->clear();
		this->widgetResult->clear();
		isFirstTime = true; // Trigger for correct UNDO.
		QStringList::Iterator it;
		for (it = inputStringArray.begin(); it != inputStringArray.end(); it++)
		{
			this->textEdit->append(*it);
		}

		if ((inputStringArray.size()) < 2)
		{
			msgBox.setWindowTitle(QStringLiteral("Внимание!"));
			msgBox.setText(QStringLiteral("Мало срок для поиска!"));
			msgBox.exec();
		}
		else
		{
			this->actionSearch->setEnabled(true);
		}
	}
}

void MainWindow::searchClicked()
{
// Check if the input array has >= 2 strings
// Check if the common substrings exist, otherwise show message
// Clean previous result
// getCommonStrings from inputStringArray
// If it empty, show the message
// (If checkbox checked, show only results of such lenght)
	if ((inputStringArray.size()) > 1)
	{
		if (!(getCommonSubstring()))
		{
			this->widgetResult->clear();
			isFirstTime = true;
			QString str;
			int lenght;
			int i = 1;
			for (QStringList::Iterator it = commonSubstringArray.begin(); it != commonSubstringArray.end(); it++)
			{
				str = *it;
				QTreeWidgetItem *item = new QTreeWidgetItem(this->widgetResult);
				item->setText(0, str);
				item->setData(1, Qt::DisplayRole, str.size());
				i++;
			}
			this->widgetResult->sortItems(1, Qt::DescendingOrder);
			this->actionSearch->setEnabled(false);
		}
		else
		{
			msgBox.setWindowTitle(QStringLiteral("Внимание!"));
			msgBox.setText(QStringLiteral("Нет общих строк."));
			msgBox.exec();
		}
	}
	else
	{
		msgBox.setWindowTitle(QStringLiteral("Внимание!"));
		msgBox.setText(QStringLiteral("Нет строк для поиска."));
		msgBox.exec();
	}
}

void MainWindow::resultClicked(QTreeWidgetItem *item, int col = 0)
{
	selectedItem = this->widgetResult->currentItem();
	searchString.clear();
	searchString = selectedItem->text(0); // String by clicked one.
	QTextDocument *document = this->textEdit->document();
	QTextCursor highlightCursor(document);
	QTextCursor qursor(document);
	if (isFirstTime == false)
	{
		document->undo(); // Reset the previous.
	}

	qursor.beginEditBlock();

	QTextCharFormat plainFormat(highlightCursor.charFormat());
	QTextCharFormat colorFormat = plainFormat;
	colorFormat.setForeground(Qt::red);
	colorFormat.setFontUnderline(true);

	while ((!highlightCursor.isNull()) && (!highlightCursor.atEnd()))
	{
		highlightCursor = document->find(searchString, highlightCursor, QTextDocument::FindCaseSensitively);
		if (!highlightCursor.isNull())
		{
			highlightCursor.movePosition(QTextCursor::NoMove, QTextCursor::KeepAnchor);
			highlightCursor.mergeCharFormat(colorFormat);
			highlightCursor.movePosition(QTextCursor::EndOfLine, QTextCursor::MoveAnchor, 1);
		}
	}

	qursor.endEditBlock();

	isFirstTime = false;
}
