﻿#pragma once

#include <QtWidgets/QMainWindow>
#include <QClipboard>
#include <qmessagebox.h>
#include <qfiledialog.h>
#include <qtextstream.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QSplitter>


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = Q_NULLPTR);
	~MainWindow();
	void generateStrings(); // Generate random strings method.
	int getCommonSubstring(); // Common substrings search method. Returns 0 if success

private:
	QStringList inputStringArray; // Input array.
	QStringList commonSubstringArray; // Array of common strings.
	QString searchString;
	QTreeWidgetItem *selectedItem;
	bool isFirstTime;
	QString testString;
	QFile fileName;

	QAction *actionQuit;
	QAction *actionGenerate;
	QAction *actionSearch;
	QAction *actionPaste;
	QAction *actionOpen;
	QAction *actionAbout;
	QMenuBar *menuBar;
	QMenu *menuFile;
	QMenu *menuEdit;
	QMenu *menuHelp;
	QToolBar *mainToolBar;
	QStatusBar *statusBar;
	QTextEdit *textEdit;
	QDockWidget *dockWidgetResult;
	QTreeWidget *widgetResult;
	QMessageBox msgBox;

private slots:
    void generateClicked();
	void openFileClicked();
	void pasteClicked();
    void searchClicked();
    void resultClicked(QTreeWidgetItem*, int);
};
